#include <fstream>
#include <iostream>

#include <cstdlib>
#include <ctime>

#include "ValueTable.h"

int main()
{
    try
    {
        std::srand(unsigned(std::time(0)));

        ValueTable valueTable;
        ValueTable dummy;
        dummy = valueTable;
        for( int i = 0; i < 15; ++i )
            valueTable.add_value( ValueTable::Value( i,  std::rand() % 20 ) );

        valueTable.sort();

        std::ofstream out( "Solution.dat" );

        valueTable.interpolation_type = ValueTable::catmullrom;

        for( double i = 0.1; i < 10; i += 0.1 )
        {
            ValueTable::Value   val = valueTable[i];
            out << val.key() << "\t" << val.value() << "\t";
            val = valueTable[(int)i];
            out << (int)i << "\t" << val.value() << std::endl;
        }

        out.close();

        system( "/usr/local/bin/gnuplot mkplot" );
    }
    catch( ChainedException* e )
    {
        std::ofstream   out( "Result.txt" );
        e->dump( out );
        std::cout << e->what();
        delete e;
    }

    return 0;
}


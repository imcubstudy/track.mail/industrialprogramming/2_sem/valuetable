#ifndef TRACK_CPP_2_SEM_VALUETABLE_H
#define TRACK_CPP_2_SEM_VALUETABLE_H

#include <cmath>
#include "Array.h"

class ValueTable
{
private:
    static const int        _stretch_factor = 2;

public:
    class Value
    {
    private:
        double  _value  = NAN;
        double  _key    = NAN;

    public:
        Value()                     = default;
        Value( const Value& that )  = default;

        Value( double key, double value ):
            _key( key ), _value( value ) {}

        ~Value()    = default;

        Value&  operator=( const Value& that ) = default;

        bool    operator<( const Value& that ) const
        { return _key < that._key; }

        double key()    { return _key; }
        double value()  { return _value; }
    };

    enum interpolation_type_t
    {
        bezier      = 0,
        catmullrom  = 1
    };

    interpolation_type_t interpolation_type = bezier;

private:
    DynamicArray<Value>     _values     = DynamicArray<Value>();

public:
    ValueTable()        = default;
    ~ValueTable()       = default;

    ValueTable( const ValueTable& that )
    {
        InvokeCthulhu( that.valid(),
          {
              _values     = that._values;
              
              interpolation_type  = that.interpolation_type;
              
          }, "Invalid rvalue in copy constructor" );
    }
    
    ValueTable( ValueTable&& that )
    {
        InvokeCthulhu( that.valid(),
          {
              _values      = std::move(that._values);
              
              interpolation_type  = that.interpolation_type;
              
          }, "Invalid rvalue in move constructor" );
    }
    
    ValueTable( const DynamicArray<Value>& values )
    {
        InvokeCthulhu( values.valid(),
          {
              _values  = values;
          }, "Given values array is invalid" );
    }

    ValueTable& operator=( const ValueTable& that )
    {
        InvokeCthulhu( that.valid(),
          {
              _values      = that._values;
              
              interpolation_type  = that.interpolation_type;
              
              return *this;
              
          }, "Invalid rvalue in copy operator=" );
    }
    
    ValueTable& operator=( ValueTable&& that )
    {
        InvokeCthulhu( that.valid(),
          {
              _values     = std::move(that._values);
              
              interpolation_type  = that.interpolation_type;
              
              return *this;
              
          }, "Invalid rvalue in move operator=" );
    }

    bool    valid() const
    { InvokeCthulhu( _values.valid(), { return true; }, "values array is invalid" ); }

    Value&  operator[]( int index )
    {
        InvokeCthulhu( valid(),
           {
               return _values[index];
           }, "ValueTable is invalid" );
    }

    Value   operator[]( double key )
    {
        #define _ ,
        InvokeCthulhu( valid(),
           {
               switch( interpolation_type )
               {
                   case bezier:
                   {
                       int start_p = 0;

                       for( start_p = 0; start_p < _values.size(); start_p += 3 )
                           if( _values[start_p].key() > key )
                           {
                               start_p -= 3;
                               break;
                           }

                       if( !( start_p < _values.size() - 4 ))
                           throw new( std::nothrow ) ChainedException( "Interpolation key out of range" );

                       Value points[] = { _values[start_p] _
                                          _values[start_p + 1] _
                                          _values[start_p + 2] _
                                          _values[start_p + 3] };

                       double t = (key - points[0].key()) / ( points[3].key() - points[0].key());

                       Value tmp1[] = { Value( t * ( points[1].key()   - points[0].key())   + points[0].key() _
                                               t * ( points[1].value() - points[0].value()) + points[0].value()) _
                                        Value( t * ( points[2].key()   - points[1].key())   + points[1].key() _
                                               t * ( points[2].value() - points[1].value()) + points[1].value()) _
                                        Value( t * ( points[3].key()   - points[2].key())   + points[2].key() _
                                               t * ( points[3].value() - points[2].value()) + points[2].value()) };

                       Value tmp2[] = { Value( t * ( tmp1[1].key()   - tmp1[0].key())   + tmp1[0].key() _
                                               t * ( tmp1[1].value() - tmp1[0].value()) + tmp1[0].value()) _
                                        Value( t * ( tmp1[2].key()   - tmp1[1].key())   + tmp1[1].key() _
                                               t * ( tmp1[2].value() - tmp1[1].value()) + tmp1[1].value()) };

                       return Value( t * ( tmp2[1].key()   - tmp2[0].key())   + tmp2[0].key() _
                                     t * ( tmp2[1].value() - tmp2[0].value()) + tmp2[0].value());
                       break;
                   }
                   case catmullrom:
                   {
                       int start_p = 0;

                       for( start_p = 0; start_p < _values.size(); start_p++ )
                           if( _values[start_p].key() > key )
                           {
                               start_p--;
                               break;
                           }

                       if( !( start_p < _values.size() - 4 ))
                           throw new( std::nothrow ) ChainedException( "Interpolation key out of range" );

                       Value points[] = { _values[start_p] _
                                          _values[start_p + 1] _
                                          _values[start_p + 2] _
                                          _values[start_p + 3] };

                       double t = key - points[0].key();

                       return Value( 0.5 * ( (-1) * t * (1 - t) * (1 - t) * points[0].key()
                                             + (2 - 5 * t * t + 3 * t * t * t) * points[1].key()
                                             + t * (1 + 4 * t - 3 * t * t) * points[2].key()
                                             - t * t * (1 - t) * points[3].key() ) _

                                     0.5 * ((-1) * t * (1 - t) * (1 - t) * points[0].value()
                                             + (2 - 5 * t * t + 3 * t * t * t) * points[1].value()
                                             + t * (1 + 4 * t - 3 * t * t) * points[2].value()
                                             - t * t * (1 - t) * points[3].value()) );
                       break;
                   }
                   default:
                   {
                       throw new( std::nothrow ) ChainedException( "Unsupported interpolation type" );
                   }
               }
           }, "Interpolation on invalid ValueTable" );
        #undef _
    }

    bool    sort()
    {
        InvokeCthulhu( valid(),
           {
               std::sort( _values.begin(), _values.end() );
               return true;

           }, "ValueTable is invalid" );
    }

    size_t  add_value( const Value& value )
    {
        InvokeCthulhu( _values.valid(),
           {
               if(      _values.capacity() == 0 )               _values.resize( 1 );
               else if( _values.capacity() == _values.size() )  _values.resize( _values.size() * _stretch_factor );

               return _values.push( value );

           }, "ValueTable is invalid" );
    }

    size_t  rm_value( int index )
    {
        InvokeCthulhu( valid(),
           {
                _values[index]  = _values.pop();
                return _values.size();
           }, "ValueTable is invalid" );
    }
};

#endif //TRACK_CPP_2_SEM_VALUETABLE_H
